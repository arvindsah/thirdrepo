package com.application.tests;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import org.testng.annotations.AfterTest;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.application.base.BasePage;
import com.application.page.InputsPage;
import com.application.page.LoginPage;
import com.application.util.AppConstants;
import com.application.util.Credentials;
import com.application.util.ExcelUtil;

public class InputsPageTest {
	
	
	BasePage  basePage;
	Properties prop;
	WebDriver driver;
	LoginPage loginPage;
	Credentials credential;
	InputsPage inputPage;
	
	
	@BeforeTest
	public void setup() {
		basePage = new BasePage();
		prop=basePage.init_properties();
		
		String browser=prop.getProperty("browser");
		String url=prop.getProperty("url");
		driver =basePage.init_driver(browser);
		driver.get(url);
		
		loginPage= new LoginPage(driver);
		inputPage=loginPage.doInputsPageNav();
		
		
		credential= new Credentials(prop.getProperty("name"), 
				prop.getProperty("pass"));
	}
	
	
	@Test(priority=1)
	public void checPageTitle() {
		System.out.println("running method inside inputs----- " + this.getClass().getName());
		String pageTitle=inputPage.getPageTitle();
		Assert.assertEquals(pageTitle, AppConstants.INPUTS_PAGE_TITLE);
	}
	
	
	@Test(priority=2)
	public void checkPageHedere() {
		
		String pageHeader=inputPage.getPageHeader();
		Assert.assertEquals(pageHeader, AppConstants.INPUTS_PAGE_HEADER);
	}

	
	@DataProvider
	public Object[][] dataForInputField(){
		Object[][] obj=ExcelUtil.readDataFromExcelSheet("inputSheet");
		//Object[][] obj = {{"5"}, {"3"}, {"3"}};
		return obj;
	}
	
	@Test(priority=3, dataProvider="dataForInputField")
	public void eneterInputsAsNumber(String numberInput) throws InterruptedException {
		inputPage.enterNumber(numberInput);
	}
	
	@AfterTest
	public void tearDown() {
		driver.quit();
	}

}
