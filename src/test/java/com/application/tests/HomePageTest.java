package com.application.tests;



import java.util.Properties;


import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.application.base.BasePage;
import com.application.page.HomePage;
import com.application.page.LoginPage;
import com.application.util.Credentials;

public class HomePageTest {
	BasePage basePage;
	Properties prop;
	WebDriver driver;
	LoginPage loginPage;
	HomePage homePage;
	Credentials credential ;

	@BeforeTest
	public void setup() {

		basePage = new BasePage();
		prop = basePage.init_properties();
		String browser = prop.getProperty("browser");
		String url = prop.getProperty("url");
		driver = basePage.init_driver(browser);
		driver.get(url);
		loginPage = new LoginPage(driver);
		credential= new Credentials(prop.getProperty("name"), 
				prop.getProperty("pass"));
		homePage = loginPage.doHomePageNav(credential);
	}

	@Test(priority = 1)
	public void verifyHomePageTitle() {
		String title = homePage.getHomePageTitle();
		System.out.println("Home page Title is : " + title);
		Assert.assertEquals(title, "The Internet");

	}

	@Test(priority = 2)
	public void verifyHeaderTitle() {
		String headerText = homePage.getHeaderText();
		System.out.println("Header Text is : " + headerText);
		//Assert.assertEquals(headerText, "A/B Test Control");
		homePage.getHeaderText();
 
	}

	@AfterTest
	public void tearDown() {
		driver.quit();
	}

}
