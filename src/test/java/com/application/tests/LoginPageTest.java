package com.application.tests;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.application.base.BasePage;
import com.application.page.HomePage;
import com.application.page.LoginPage;
import com.application.util.AppConstants;
import com.application.util.Credentials;


/**
 * 
 * @author Arvind
 *
 */
public class LoginPageTest {

	BasePage  basePage;
	Properties prop;
	WebDriver driver;
	LoginPage loginPage;
	Credentials credential;
	
	@BeforeTest
	public void setup() {
		basePage = new BasePage();
		prop=basePage.init_properties();
		
		String browser=prop.getProperty("browser");
		String url=prop.getProperty("url");
		driver =basePage.init_driver(browser);
		
		driver.get(url);
		
		loginPage= new LoginPage(driver);
		credential= new Credentials(prop.getProperty("name"), 
				prop.getProperty("pass"));
		
		
	}
	
	@Test(priority = 1)
	public void verifyTitleOfPage() {
		String pageTitle=loginPage.getPageTitle();
		System.out.println("Title of the Login page : "+pageTitle);
		Assert.assertEquals(pageTitle, AppConstants.LOGIN_PAGE_TITLE);
	}
	
	@Test(priority = 2)
	public void verifyABLinkPresent() {
		boolean abLinkPresent=loginPage.checkABTestingLink();
		System.out.println("AB link present : "+abLinkPresent);
		Assert.assertTrue(abLinkPresent);
		//Assert.assertEquals(abLinkPresent, true);
	}
	
	
	/**
	 * this methoed will help you naviagte to home page
	 * if required get the logged user name 
	 * which is displayed at home from prop file
	 * 
	 */
	@Test(priority = 3)
	public void homePageNav() {
		
		HomePage homePage=loginPage.doHomePageNav(credential);
		String homePageTile=homePage.getHomePageTitle();
		System.out.println("Home page title is :" +homePageTile);
		Assert.assertEquals(homePageTile, AppConstants.HOME_PAGE_TITLE);
	}
	
	@DataProvider
	public Object[][] getinvalidLoginCred() {
		
		Object obj[][] = {
				{"login1","pass1"},
				{"login2","pass2"},
				{"login3","pass3"}
		};
		
		return obj;
	}
	
	@Test(priority=4, dataProvider = "getinvalidLoginCred", enabled=false)
	public void invalidLoginTest(String username, String pass) {
		credential.setName(username);
		credential.setPassword(pass);
		
		HomePage homePage=loginPage.doHomePageNav(credential);
		String homePageTile=homePage.getHomePageTitle();
		System.out.println("Home page title is :" +homePageTile);
		Assert.assertEquals(homePageTile, AppConstants.HOME_PAGE_TITLE);
				
	}
	
	
	@Test(priority=5)
	public void verifyPageTitle()
	{
		String pageTitle= loginPage.getPageTitleUsingJS();
		System.out.println("page title using JS :"+pageTitle );
		Assert.assertEquals(pageTitle, AppConstants.LOGIN_PAGE_TITLE);
	}
	@AfterTest
	public void tearDown() {
		driver.quit();
	}
}
