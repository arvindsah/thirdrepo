package com.application.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import io.github.bonigarcia.wdm.WebDriverManager;


public class BasePage {
	
	//intilize the driver and prop
	
	WebDriver driver;
	Properties prop;
	public static boolean highlightElement; 
	OptionsManager optionsManager;
	
	public WebDriver init_driver(String browerName) {
		System.out.println("browser name is : " + browerName);
	
		highlightElement = prop.getProperty("highlight").equals("true") ? true : false;
		optionsManager= new OptionsManager(prop);
	
		if (browerName.equals("chrome")) {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver(optionsManager.getChromeOptions());
		}

		else if (browerName.equals("firefox")) {
			
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver(optionsManager.getFirefoxOptions());
			}

		else if (browerName.equals("safari")) {
			WebDriverManager.getInstance(SafariDriver.class).setup();
			driver = new SafariDriver();
		}

		else {
			System.out.println("browser name : " + browerName + " not found");
		}

		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		// driver.get(url);

		return driver;

	}

	
	public Properties init_properties() {
		prop= new Properties();
		//String path= ".\\src\\main\\java\\com\\application\\config\\config.properties";
		String env=null;
		String path=null;
		
		try {
			env=System.getProperty("env");
			if(env.equals("qa"))
			{
				path=".\\src\\main\\java\\com\\application\\config\\config.qa.properties";
			}
		}
		catch(Exception e)
		{
			path=".\\src\\main\\java\\com\\application\\config\\config.properties";
		}
		
		try {
			FileInputStream fileInputStream= new FileInputStream(path);
			prop.load(fileInputStream);
		} catch (FileNotFoundException e) {
			System.out.println("unable to read config properies");
		} catch (IOException e) {
			System.out.println("unable to load properties file");
		}
		return prop;
	}
}
